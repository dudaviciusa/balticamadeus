import XCTest
@testable import Amadeus

class AmadeusTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testGetPosts() {
        
        var testPostResponse: [BMPost]?
        let expectation = XCTestExpectation(description: "iban information should be not nil")
        
        let postApiClient = PostApiClientFactory.createTransferApiClient()
        
        
        postApiClient.getPosts().done { response in
            testPostResponse = response
            expectation.fulfill()
            }.catch { error in
                print((error as? BMPostApiError)?.toJSON() ?? "")
                expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 677777.0)
        XCTAssertNotNil(testPostResponse)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
