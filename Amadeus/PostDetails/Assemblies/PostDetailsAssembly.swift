import Swinject

class PostDetailsAssembly: Assembly {
    
    func assemble(container: Container) {

        container.register(PostDetailsRouter.self) { _ in
            return PostDetailsRouter()
            }.inObjectScope(.container)
        
        container.register(PostDetailsViewController.self) { _, postDetailsViewModel in
            return PostDetailsViewController(postDetailsViewModel: postDetailsViewModel)
        }.inObjectScope(.transient)
        
        
        container.register(PostDetailsViewModel.self) { r, post in
            return PostDetailsViewModel(post: post,
                                        postSynchronizationManager: r.resolve(PostSynchronizationManager.self)!)
            }.inObjectScope(.transient)
        
        
        container.register(BaseXibView.self, name: "PostDetailsView", factory: { _, frame, postDetailsViewModel in
            return PostDetailsView(
                frame: frame,
                postDetailsViewModel: postDetailsViewModel)
        }).inObjectScope(.transient)
    }
}
