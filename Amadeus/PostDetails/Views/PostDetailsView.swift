import UIKit
import RxSwift
import Kingfisher

class PostDetailsView: BaseXibView {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var adressButton: UIButton!
    @IBOutlet weak var userPhoneButton: UIButton!
    @IBOutlet weak var userCompanyLabel: UILabel!
    @IBOutlet weak var postBodyLabel: UILabel!
    @IBOutlet weak var userAvatarView: UIImageView!
    
    
    private let disposeBag = DisposeBag()
    private let postDetailsViewModel: PostDetailsViewModel
    private let refreshControl = UIRefreshControl()
    
    required init(frame: CGRect, postDetailsViewModel: PostDetailsViewModel) {
        self.postDetailsViewModel = postDetailsViewModel
        
        super.init(frame: frame)
        
        if #available(iOS 10.0, *) {
            scrollView.refreshControl = refreshControl
        } else {
            scrollView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }
    
    override func onViewInitialized() {
        super.onViewInitialized()
        
        
        postDetailsViewModel
            .postRx
            .asObservable()
            .subscribe { [weak self] post in
                self?.refreshControl.endRefreshing()
                guard let post = post.element else { return }
                self?.update(post: post)
                
        }.disposed(by: disposeBag)
        update(post: postDetailsViewModel.post)
        userAvatarView.backgroundColor = .white
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        userAvatarView.setRoundedCorners(cornerRadius: 46, cornerColor: .gray, borderWidth: 0.75)
        userAvatarView.layoutIfNeeded()
    }
    
    @IBAction func addressButtonTapped(_ sender: Any) {
        guard (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) else {
            print("Can't use comgooglemaps://")
            return
        }
        guard let lat = postDetailsViewModel.post.users.first?.address?.lat,
            let lng = postDetailsViewModel.post.users.first?.address?.lng,
            let url = URL(string: "comgooglemaps://?saddr=&daddr=\(lat),\(lng)&directionsmode=driving") else {
            print("maps url is invalid")
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    @IBAction func phoneButtonTapped(_ sender: Any) {
        guard let phone = postDetailsViewModel.post.users.first?.phone else {
            print("phone not found")
            return
        }
        
        guard let url = URL(string: "TEL://\(phone.withoutSpaces())") else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    private func update(post: Post) {
        userNameLabel.text = post.users.first?.name
        userEmailLabel.text = post.users.first?.email
        userPhoneButton.setTitle(post.users.first?.phone, for: .normal) 
        userCompanyLabel.text = post.users.first?.companies.first?.name
        postBodyLabel.text = post.body.withoutNewLines()
        adressButton.setTitle(post.users.first?.getCompleteAddress(), for: .normal)
        if let userId = post.users.first?.id {
            let url = URL(string: "https://source.unsplash.com/collection/542909/?sig=\(userId)")
            userAvatarView.kf.setImage(with: url)
        }
    }
    
    @objc private func refreshData(_ sender: Any) {
        refreshControl.beginRefreshing()
        postDetailsViewModel.synchronizePost()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
