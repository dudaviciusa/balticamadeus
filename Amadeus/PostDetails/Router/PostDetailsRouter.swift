import Foundation

class PostDetailsRouter: Router {
    
    func showPostDetailsController(post: Post) {
        
        let postDetailsViewModel = MainAssembly.resolve(PostDetailsViewModel.self, argument: post)
        let postDetailsViewController = MainAssembly.resolve(PostDetailsViewController.self, argument: postDetailsViewModel)
        
        show(postDetailsViewController)
    }
}
