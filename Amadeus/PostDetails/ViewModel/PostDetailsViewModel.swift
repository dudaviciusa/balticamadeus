import Foundation
import RxSwift

class PostDetailsViewModel: BaseViewModel {
    
    private let postSynchronizationManager: PostSynchronizationManager
    
    let postRx: Variable<Post>
    var post: Post {
        get {
            return postRx.value
        }
        set {
            postRx.value = newValue
        }
    }
    
    init(post: Post,
         postSynchronizationManager: PostSynchronizationManager) {
        
        self.postRx = Variable<Post>(post)
        self.postSynchronizationManager = postSynchronizationManager
    }
    
    func synchronizePost() {
        postSynchronizationManager
            .synchronizePost(id: post.id)
            .done { [weak self] post in
                self?.post = post
        }
    }
}
