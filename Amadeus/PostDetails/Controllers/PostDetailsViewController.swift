import Foundation

class PostDetailsViewController: BaseViewController {
    
    private let postDetailsViewModel: PostDetailsViewModel
    
    init(postDetailsViewModel: PostDetailsViewModel) {
        self.postDetailsViewModel = postDetailsViewModel
        
        super.init(viewModel: postDetailsViewModel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = postDetailsViewModel.post.title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
