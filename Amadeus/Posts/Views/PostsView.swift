import Foundation
import UIKit
import RxSwift
import RxDataSources

class PostsView: BaseXibView {
    
    private let postsViewModel: PostsViewModel
    private let disposeBag = DisposeBag()
    private let refreshControl = UIRefreshControl()
    private var justSelectedPost: Post?
    @IBOutlet weak var postsTableView: UITableView!
    
    required init(frame: CGRect, postsViewModel: PostsViewModel) {
        self.postsViewModel = postsViewModel
        
        super.init(frame: frame)
        postsTableView.register(PostTableViewCell.self, forCellReuseIdentifier: "PostTableViewCell")
        
        if #available(iOS 10.0, *) {
            postsTableView.refreshControl = refreshControl
        } else {
            postsTableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }
    
    func initPostTableView() {
        postsTableView.rowHeight = UITableView.automaticDimension
        refreshControl.beginRefreshing()
        
        postsViewModel
            .postsRx
            .asObservable()
            .bind(to: postsTableView.rx.items(cellIdentifier: "PostTableViewCell") ) { [weak self] _, post, cell in
                self?.refreshControl.endRefreshing()
                let postCell = cell as! PostTableViewCell
                postCell.populate(post: post)
        }.disposed(by: disposeBag)
        
        
        postsTableView.rx.itemSelected.subscribe  { [weak self] indexPath  in
            guard let `self` = self,
                let tableIndexPath = indexPath.element,
                tableIndexPath.row < self.postsViewModel.posts.count else {
                    return
            }
            guard self.justSelectedPost != self.postsViewModel.posts[tableIndexPath.row] else {
                return
            }

            self.justSelectedPost = self.postsViewModel.posts[tableIndexPath.row]
            self.postsViewModel.onPostItemTappedRx.value = self.justSelectedPost
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4, execute: { [weak self] in
                self?.justSelectedPost = nil
                self?.postsTableView.deselectRow(at: tableIndexPath, animated: true)
            })
            
        }.disposed(by: disposeBag)
    }
    
    @objc private func refreshData(_ sender: Any) {
        postsViewModel.synchrronizePostsData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
