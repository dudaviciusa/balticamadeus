//
//  PostViewCell.swift
//  Amadeus
//
//  Created by Arvydas Dudavicius on 21/02/2019.
//  Copyright © 2019 Arvydas Dudavicius. All rights reserved.
//

import UIKit

class PostTableViewCell: BaseTableViewCell {

//    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var bodyLabel: UILabel!
    
    private let userInfoStackView = UIStackView()
    private let postInfoStackView = UIStackView()
    private let userNameLabel = UILabel()
    private let companyLabel = UILabel()
    private let titleLabel = UILabel()
    private let bodyLabel = UILabel()

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func loadViewFromNib() -> UIView? {
        // i would prefere to don't use xib cell
        return nil
    }
    
    override func render() {
        // better solution is to use Stevia or other layout library
        sv(
            userInfoStackView,
            postInfoStackView
        )
        layout (
            userInfoStackView.top(6).left(18).right(18),
            8,
            postInfoStackView.left(18).right(18).bottom(6)
        )
        userInfoStackView.addArrangedSubview(userNameLabel.style(userLabelsStyle))
        userInfoStackView.addArrangedSubview(companyLabel.style(userLabelsStyle))
        userInfoStackView.axis = .horizontal
        userInfoStackView.spacing = 12
        
        postInfoStackView.addArrangedSubview(titleLabel.style(titleLabelStyle))
        postInfoStackView.addArrangedSubview(bodyLabel.style(bodyLabelStyle))
        postInfoStackView.axis = .vertical
        postInfoStackView.spacing = 4
    }
    
    override func getCellFrame() -> CGRect? {
        return nil
    }
    
    func populate(post: Post) {
        
        userNameLabel.text = post.users.first?.username
        companyLabel.text = post.users.first?.companies.first?.name
        titleLabel.text = post.title.withoutNewLines()
        bodyLabel.text = post.body.withoutNewLines()
    }
    
    private func clear() {
        userNameLabel.text = nil
        companyLabel.text = nil
        titleLabel.text = nil
        bodyLabel.text = nil
    }
    
    private func userLabelsStyle(label: UILabel) {
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        label.textColor = UIColor(red: 44/255, green: 156/255, blue: 77/255, alpha: 1)
        label.textAlignment = .left
        label.numberOfLines = 0
    }
    
    private func titleLabelStyle(label: UILabel) {
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        label.textColor = UIColor.black.withAlphaComponent(0.8)
        label.textAlignment = .left
        label.numberOfLines = 0
    }
    
    private func bodyLabelStyle(label: UILabel) {
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        label.textColor = .gray
        label.textAlignment = .left
        label.numberOfLines = 0
    }
}
