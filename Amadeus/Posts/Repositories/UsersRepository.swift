import RealmSwift

class UsersRepository: EntityRepository {
    
    func findAll() -> Results<User> {
        return self.realm.objects(User.self)
    }
    
    func find(_ id: Int) -> User? {
        return self.realm.object(ofType: User.self, forPrimaryKey: id)
    }
}
