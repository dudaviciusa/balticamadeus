import RealmSwift

class PostsRepository: EntityRepository {
    
    func findAll() -> Results<Post> {
        return self.realm.objects(Post.self)
    }
}
