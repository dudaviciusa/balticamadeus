import Foundation
import RealmSwift
import PromiseKit

class UsersSynchronizationManager {
    
    private let postApiClient: PostApiClient
    private let usersRepository: UsersRepository
    
    var realm: Realm! {
        return try! Realm()
    }
    
    init(postApiClient: PostApiClient,
         usersRepository: UsersRepository
        ) {
        self.postApiClient = postApiClient
        self.usersRepository = usersRepository
    }
    
    func synchronizeUsers() -> Promise<[User]> {
        return postApiClient
            .getUsers()
            .then{ apiUsers -> Promise<[User]> in
                
                self.deleteNotExistedUsers(compatedTo: apiUsers)
                let users = self.updateUsers(with: apiUsers)
                return Promise<[User]> { $0.fulfill(users) }
        }
    }
    
    func synchronizeUser(id: Int) -> Promise<User> {
        return postApiClient
            .getUser(userId: id)
            .then{ apiUser -> Promise<User> in
                
                let users = self.updateUsers(with: [apiUser])
                return Promise<User> { $0.fulfill(users.first!) }
        }
    }
    
    private func deleteNotExistedUsers(compatedTo apiUsers: [BMUser]) {
        
        self.usersRepository
            .findAll()
            .filter { user in
                !apiUsers.contains { user.id == $0.id }
            }.forEach { self.realm.delete($0) }
    }
    
    private func updateUsers(with apiUsers: [BMUser]) -> [User] {
        let users = apiUsers.map { self.createUser(apiUser: $0) }
        try! self.realm.write {
            self.realm.add(users, update: true)
        }
        return users
    }
    
    private func createUser(apiUser: BMUser) -> User {
        let user = User()
        user.id = apiUser.id
        user.name = apiUser.name
        user.username = apiUser.username
        user.email = apiUser.email
        user.phone = apiUser.phone
        user.website = apiUser.website
        
        let company = createCompany(apiCompany: apiUser.company)
        user.companies.append(company)
        user.address = createAddress(apiAddress: apiUser.address)
        
        return user
    }
    
    private func createCompany(apiCompany: BMCompany) -> Company {
        let company = Company()
        company.name = apiCompany.name
        company.catchPhrase = apiCompany.catchPhrase
        company.bs = apiCompany.bs
        return company
    }
    
    private func createAddress(apiAddress: BMAddress) -> Address {
        let address = Address()
        address.street = apiAddress.street
        address.suite = apiAddress.suite
        address.city = apiAddress.city
        address.zipcode = apiAddress.zipcode
        address.lat = apiAddress.geo.lat
        address.lng = apiAddress.geo.lng
        return address
    }
}
