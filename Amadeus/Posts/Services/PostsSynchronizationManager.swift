import Foundation
import RealmSwift
import PromiseKit

class PostSynchronizationManager {
    
    private let postApiClient: PostApiClient
    private let postsRepository: PostsRepository
    private let usersSynchronizationManager: UsersSynchronizationManager
    
    var realm: Realm! {
        return try! Realm()
    }
    
    init(postApiClient: PostApiClient,
         postsRepository: PostsRepository,
         usersSynchronizationManager: UsersSynchronizationManager
        ) {
        self.postApiClient = postApiClient
        self.postsRepository = postsRepository
        self.usersSynchronizationManager = usersSynchronizationManager
    }
    
    func synchronizePost(id: Int) -> Promise<Post> {
        
        return postApiClient
            .getPost(postId: id)
            .then{ apiPost -> Promise<Post> in
                
                let posts = self.updatePosts(with: [apiPost])
                return Promise<Post> { $0.fulfill(posts.first!) }
                    .then{ post -> Promise<Post> in
                        
                        return Promise<Post> { value in
                            self.usersSynchronizationManager
                                .synchronizeUser(id: post.userId)
                                .done { user in
                                    self.addRelationships(posts: [post], with: [user])
                                    value.fulfill(post)
                            }
                        }
                }
        }
    }
    
    func synchronizePosts() -> Promise<[Post]> {
        return postApiClient
            .getPosts()
            .then{ apiPosts -> Promise<[Post]> in
                
                // deletes no longer relevant Posts
                self.deleteNotExistedPosts(compatedTo: apiPosts)
                let posts = self.updatePosts(with: apiPosts)
                return Promise<[Post]> { $0.fulfill(posts) }
            .then{ posts -> Promise<[Post]> in
                
                return Promise<[Post]> { value in
                self.usersSynchronizationManager
                    .synchronizeUsers()
                    .done { users in
                        self.addRelationships(posts: posts, with: users)
                        value.fulfill(posts)
                    }
                }
            }
        }
    }
    
    private func deleteNotExistedPosts(compatedTo apiPosts: [BMPost]) {
        
        self.postsRepository
            .findAll()
            .filter { post in
                !apiPosts.contains { post.id == $0.id }
            }.forEach { self.realm.delete($0) }
    }
    
    private func updatePosts(with apiPosts: [BMPost]) -> [Post] {
        let posts = apiPosts.map { self.createPost(apiPost: $0) }
        try! self.realm.write {
            self.realm.add(posts, update: true)
        }
        return posts
    }
    
    private func addRelationships(posts: [Post], with users: [User]) {
        
        try! self.realm.write {
            posts.forEach { post in
                users.forEach { user in
                    if post.userId == user.id {
                        post.users.append(user)
                    }
                }
            }
        }
    }
    
    private func createPost(apiPost: BMPost) -> Post {
        let post = Post()
        post.id = apiPost.id
        post.userId = apiPost.userId
        post.title = apiPost.title
        post.body = apiPost.body
        return post
    }
}
