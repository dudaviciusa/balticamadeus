import Foundation
import RxSwift

class PostsViewModel: BaseViewModel {
    
    private let postsRepository: PostsRepository
    private let postSynchronizationManager: PostSynchronizationManager
    
    var onPostItemTappedRx = Variable<Post?>((nil))
    let postsRx: Variable<[Post]>
    var posts: [Post] {
        get {
            return postsRx.value
        }
        set {
            postsRx.value = newValue
        }
    }
    
    init(postsRepository: PostsRepository,
         postSynchronizationManager: PostSynchronizationManager
        ) {
        self.postsRepository = postsRepository
        self.postSynchronizationManager = postSynchronizationManager
        self.postsRx = Variable<[Post]>(Array(postsRepository.findAll()))
        super.init()
        
        synchrronizePostsData()
    }
    
    func synchrronizePostsData() {
        postSynchronizationManager
            .synchronizePosts()
            .done { posts in
                self.postsRx.value = posts
        }
    }
}
