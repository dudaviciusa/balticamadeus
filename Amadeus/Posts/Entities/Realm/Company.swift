import RealmSwift
import Foundation

class Company: Object {
    
    @objc dynamic var catchPhrase = ""
    @objc dynamic var name = ""
    @objc dynamic var bs = ""
    
    let users = LinkingObjects(fromType: User.self, property: "companies")
    
    override static func primaryKey() -> String? {
        return "name"
    }
}
