import RealmSwift
import Foundation

class Address: Object {
    
    @objc dynamic var street = ""
    @objc dynamic var suite = ""
    @objc dynamic var city = ""
    @objc dynamic var zipcode = ""
    @objc dynamic var lat = ""
    @objc dynamic var lng = ""
    
    override static func primaryKey() -> String? {
        return "zipcode"
    }
}

