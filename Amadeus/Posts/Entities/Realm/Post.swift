import RealmSwift
import Foundation

class Post: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var userId = 0
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    
    var users = List<User>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
