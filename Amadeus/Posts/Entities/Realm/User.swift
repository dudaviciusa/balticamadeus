import RealmSwift
import Foundation

class User: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var username = ""
    @objc dynamic var email = ""
    @objc dynamic var phone = ""
    @objc dynamic var website = ""
    @objc dynamic var address: Address?
    
    var companies = List<Company>()
    let posts = LinkingObjects(fromType: Post.self, property: "users")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func getCompleteAddress() -> String {
        guard let address = self.address else { return "has no address" }
        return address.street + " " + address.suite + " " + address.city
    }
}
