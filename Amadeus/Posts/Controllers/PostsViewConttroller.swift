import Foundation
import RxSwift

class PostsViewController: BaseViewController {
    
    private let postsViewModel: PostsViewModel
    private let disposeBag = DisposeBag()
    private let postDetailsRouter: PostDetailsRouter
    
    
    init(postsViewModel: PostsViewModel, postDetailsRouter: PostDetailsRouter) {
        self.postsViewModel = postsViewModel
        self.postDetailsRouter = postDetailsRouter
        
        super.init(viewModel: postsViewModel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Posts"
        postsViewModel
            .onPostItemTappedRx
            .asObservable()
            .subscribe { [weak self] post in
                guard let `self` = self,
                    let postElement = post.element,
                    let selectedPost = postElement else { return }
                
                self.postDetailsRouter.showPostDetailsController(post: selectedPost)
        }.disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getView().initPostTableView()
    }
    
    override func getView() -> PostsView {
        return self.view as! PostsView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
