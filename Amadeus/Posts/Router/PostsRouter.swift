import UIKit


class PostsRouter: Router {
    
    func presentPostsViewController() {
        
        let postsViewController = MainAssembly.resolve(PostsViewController.self)
        setAndPresent(rootViewController: UINavigationController(rootViewController: postsViewController),
                      animation: .transitionCrossDissolve,
                      duration: 0.2)
    }
}
