import Swinject

class PostsAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(PostsRouter.self) { _ in
            return PostsRouter()
        }.inObjectScope(.container)
        
        container.register(PostsViewModel.self) { r in
            return PostsViewModel(postsRepository: r.resolve(PostsRepository.self)!,
                                  postSynchronizationManager: r.resolve(PostSynchronizationManager.self)!)
            }.inObjectScope(.transient)
        
        container.register(PostsViewController.self) { r in
            return PostsViewController(postsViewModel: r.resolve(PostsViewModel.self)!,
                                       postDetailsRouter: r.resolve(PostDetailsRouter.self)!)
            }.inObjectScope(.transient)
        
        container.register(PostsRepository.self) { r in
            return PostsRepository()
            }.inObjectScope(.container)
        
        container.register(UsersRepository.self) { r in
            return UsersRepository()
            }.inObjectScope(.container)
        
        container.register(PostSynchronizationManager.self) { r in
            return PostSynchronizationManager(postApiClient: r.resolve(PostApiClient.self)!,
                                              postsRepository: r.resolve(PostsRepository.self)!,
                                              usersSynchronizationManager: r.resolve(UsersSynchronizationManager.self)!)
            }.inObjectScope(.container)
        
        container.register(UsersSynchronizationManager.self) { r in
            return UsersSynchronizationManager(postApiClient: r.resolve(PostApiClient.self)!,
                                               usersRepository: r.resolve(UsersRepository.self)!)
            }.inObjectScope(.container)
        
        container.register(BaseXibView.self, name: "PostsView", factory: { _, frame, viewModel in
            return PostsView(
                frame: frame,
                postsViewModel: viewModel)
            }).inObjectScope(.transient)
    }
}
