import UIKit


class Router {
    
    func setAndPresent(rootViewController: UIViewController,
                       animation: UIView.AnimationOptions,
                       duration: TimeInterval) {
        
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        
        UIView.transition(
            with: appDelegate.window!,
            duration: duration,
            options: animation,
            animations: {
                appDelegate.window?.rootViewController = rootViewController
                appDelegate.window?.makeKeyAndVisible()
        }) { finish in
            
        }
    }
    
    func present(_ controller: UIViewController, animated: Bool = true, completion: (() -> Swift.Void)? = nil) {
        
        let topMostController = UIViewController.topmost()
        controller.modalPresentationStyle = .overCurrentContext
        topMostController.present(controller, animated: animated, completion: completion)
    }
    
    func show(_ controller: UIViewController) {
        if let topMostControllerNavigationController = UIViewController.topmost() as? UINavigationController {
            topMostControllerNavigationController.show(controller, sender: nil)
            controller.viewDidLoad()
        } else {
            let navigationController = UINavigationController()
            navigationController.addChild(controller)
            UIViewController.topmost().show(navigationController, sender: nil)
        }
    }
}

