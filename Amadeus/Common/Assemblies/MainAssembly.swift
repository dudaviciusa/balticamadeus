import Swinject


class MainAssembly {
    
    static let assembler = Assembler([
        PostsAssembly(),
        ApiAssembly(),
        PostDetailsAssembly()
    ])
    
    class func resolve<Service>(_ serviceType: Service.Type) -> Service {
        return assembler.resolver.resolve(serviceType)!
    }
    
    class func resolve<Service, Arg>(_ serviceType: Service.Type, argument: Arg) -> Service {
        return assembler.resolver.resolve(serviceType, argument: argument)!
    }
    
    class func resolve<Service, Arg, Arg2>(_ serviceType: Service.Type, name: String, argument: Arg, argument2: Arg2) -> Service {
        return assembler.resolver.resolve(serviceType, name: name, arguments: argument, argument2)!
    }
}
