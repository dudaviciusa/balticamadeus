import ObjectMapper
import RealmSwift

class MappableRealmObject: Object, Mappable {
    
    required convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) { }

}
