import UIKit

extension UIViewController {
    
    class func topmost() -> UIViewController {
        var topController = self.rootViewController()
        
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    class func rootViewController() -> UIViewController {
        
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.window!.rootViewController!
    }
    
}
