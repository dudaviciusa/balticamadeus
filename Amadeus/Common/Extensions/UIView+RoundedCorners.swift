import Foundation
import UIKit

extension UIView {
    
    func setRoundedCorners(cornerRadius: CGFloat, cornerColor: UIColor = .white, borderWidth: CGFloat)  {
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
        layer.masksToBounds = true
        if cornerColor != .clear {
            addBorders(color: cornerColor, borderWidth: borderWidth)
        }
    }
    
    func addBorders(color: UIColor, borderWidth: CGFloat) {
        layer.borderWidth = borderWidth
        layer.borderColor = color.cgColor
    }
}
