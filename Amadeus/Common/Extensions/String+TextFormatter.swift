
import Foundation

extension String {
    
    func withoutNewLines() -> String {
        return replacingOccurrences(of: "\n", with: "")
    }
    
    func withoutSpaces() -> String {
        return replacingOccurrences(of: " ", with: "")
    }
}
