import UIKit

class BaseViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private var preparedView: UIView?
    
//    init() {
//        super.init(nibName: nil, bundle: nil)
//    }
    
    init<T: BaseViewModel>(viewModel: T) {
        super.init(nibName: nil, bundle: nil)
        self.preparedView = self.createView(with: viewModel)
    }
    
    override func loadView() {
        self.view = preparedView ?? self.createView()
    }
    
    private func createView() -> UIView {
        let classFromName = NSClassFromString(getViewClassName()) as! UIView.Type
        return classFromName.init(frame: self.getFrame())
    }
    
    private func createView<T: BaseViewModel>(with viewModel: T) -> BaseXibView {
        let classFromName = NSClassFromString(getViewClassName()) as! BaseXibView.Type
        return MainAssembly.resolve(classFromName, name: getViewName(), argument: self.getFrame(), argument2: viewModel)
    }
    
    private func getViewName() -> String {
        return String(describing: self.classForCoder).replacingOccurrences(of: "Controller", with: "")
    }
    
    private func getViewClassName() -> String {
        let viewName = getViewName()
        let object = NSStringFromClass(BaseViewController.self) as NSString
        let module = object.components(separatedBy: ".").first!
        return "\(module).\(viewName)"
    }
    
    func setClearStyleForNavigationBar() {
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.backgroundColor = .white
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func getView() -> UIView {
        return self.view
    }
    
    func getFrame() -> CGRect {
        return UIScreen.main.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func onBackTapped() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
}

extension UIViewController {
    func createLeftBarButtonItem(_ title: String) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(onLeftBarButtonTapped))
    }
    
    func createRightBarButtonItem(_ title: String) {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(onRightBarButtonTapped))
        self.navigationItem.rightBarButtonItem?.tintColor = .red
    }
    
    @objc func onLeftBarButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onRightBarButtonTapped() {
    }
    
    func isTopViewControllerInNavigation() -> Bool  {
        return navigationController?.viewControllers.first == self
    }
}
