import UIKit

class BaseXibView: UIView {
    
    @objc var view: UIView!
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        layoutIfNeeded()
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: self.getXibName(), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func getXibName() -> String {
        return String(describing: self.classForCoder)
    }
    
    func onViewInitialized() {
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        self.onViewInitialized()
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        return self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        self.onViewInitialized()
    }
}
