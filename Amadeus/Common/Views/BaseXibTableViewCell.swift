import UIKit
import Foundation
import Stevia

class BaseTableViewCell: UITableViewCell {
    
    var xibView: UIView?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        if let xibView = loadViewFromNib() {
            self.xibView = xibView
            self.addSubview(xibView)
        } else {
            render()
        }
    }
    
    // render layout programmatically
    func render() {
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: self.getXibName(), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    func getXibName() -> String {
        return String(describing: self.classForCoder)
    }
    
    func getCellFrame() -> CGRect? {
        return frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
