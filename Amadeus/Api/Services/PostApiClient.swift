import Foundation
import Alamofire
import PromiseKit
import ObjectMapper

class PostApiClient {
    
    private let sessionManager: SessionManager
    
    init(sessionManager: SessionManager) {
        self.sessionManager = sessionManager
    }
    
    func getPosts() -> Promise<[BMPost]> {
        return doRequest(requestRouter: PostApiRequestRouter.getPosts())
    }
    
    func getUsers() -> Promise<[BMUser]> {
        return doRequest(requestRouter: PostApiRequestRouter.getUsers())
    }
    
    func getPost(postId: Int) -> Promise<BMPost> {
        return doRequest(requestRouter: PostApiRequestRouter.getPost(postId))
    }
    
    func getUser(userId: Int) -> Promise<BMUser> {
        return doRequest(requestRouter: PostApiRequestRouter.getUser(userId))
    }
    
    // MARK: - Private request methods
    private func makeRequest(apiRequest: ApiRequest) {
        
        sessionManager
            .request(apiRequest.requestEndPoint)
            .responseJSON { (response) in
                let responseData = response.result.value
                
                guard let statusCode = response.response?.statusCode else {
                    let error = self.mapError(body: responseData)
                    apiRequest.pendingPromise.resolver.reject(error)
                    return
                }
                if statusCode >= 200 && statusCode < 300 {
                    apiRequest.pendingPromise.resolver.fulfill(responseData)
                } else {
                    apiRequest.pendingPromise.resolver.reject(self.mapError(body: responseData))
                }
        }
    }
    
    private func doRequest<RC: URLRequestConvertible, E: Mappable>(requestRouter: RC) -> Promise<[E]> {
        let request = createRequest(requestRouter)
        makeRequest(apiRequest: request)
        
        return request
            .pendingPromise
            .promise
            .then(createPromiseWithArrayResult)
    }
    
    private func doRequest<RC: URLRequestConvertible, E: Mappable>(requestRouter: RC) -> Promise<E> {
        let request = createRequest(requestRouter)
        makeRequest(apiRequest: request)
        
        return request
            .pendingPromise
            .promise
            .then(createPromise)
    }
    
    private func createRequest<T: ApiRequest, R: URLRequestConvertible>(_ endpoint: R) -> T {
        
        return T.init(pendingPromise: Promise<Any>.pending(),
                      requestEndPoint: endpoint)
    }
    
    private func createPromiseWithArrayResult<T: Mappable>(body: Any) -> Promise<[T]> {
        guard let objects = Mapper<T>().mapArray(JSONObject: body) else {
            return Promise(error: mapError(body: body))
        }
        return Promise.value(objects)
    }
    
    private func createPromise<T: Mappable>(body: Any) -> Promise<T> {
        guard let object = Mapper<T>().map(JSONObject: body) else {
            return Promise(error: mapError(body: body))
        }
        return Promise.value(object)
    }
    
    private func createPromise(body: Any) -> Promise<Any> {
        return Promise.value(body)
    }
    
    private func mapError(body: Any?) -> BMPostApiError {
        if let apiError = Mapper<BMPostApiError>().map(JSONObject: body) {
            return apiError
        }
        
        return BMPostApiError.unknown()
    }
}
