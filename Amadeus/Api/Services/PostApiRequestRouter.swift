import Foundation
import Alamofire

enum PostApiRequestRouter: URLRequestConvertible {
    
    // MARK: - GET
    case getPosts()
    case getUsers()
    case getUser(_ userId: Int)
    case getPost(_ postId: Int)
    
    
    static var baseURLString = "https://jsonplaceholder.typicode.com"
    
    private var method: HTTPMethod {
        switch self {
        case .getPosts(),
             .getUsers(),
             .getUser( _),
             .getPost( _):
            return .get
        }
    }
    
    private var path: String {
        switch self {
        case .getPosts():
            return "/posts"
        case .getUsers():
            return "/users"
        case .getUser(let userId):
            return "/users/\(userId)"
        case .getPost(let postId):
            return "/posts/\(postId)"
        }
    }
    
    private var parameters: Parameters? {
        switch self {
        default:
            return nil
        }
    }
    
    // MARK: - Method
    func asURLRequest() throws -> URLRequest {
        let url = try! PostApiRequestRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        default:
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
