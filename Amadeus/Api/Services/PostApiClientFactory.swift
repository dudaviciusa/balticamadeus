import Foundation
import Alamofire

class PostApiClientFactory {
    
    public static func createTransferApiClient() -> PostApiClient {
        let sessionManager = SessionManager()
        sessionManager.adapter = PostRequestAdapter()
        
        return PostApiClient(sessionManager: sessionManager)
    }
}
