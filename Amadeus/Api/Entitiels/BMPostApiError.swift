import ObjectMapper

class BMPostApiError: Mappable, Error {
    var error: String
    var errorDescription: String?
    var statusCode: Int?
    
    init(error: String, description: String? = nil) {
        self.error = error
        self.errorDescription = description
    }
    
    required init?(map: Map) {
        do {
            error = try map.value("error")
        } catch {
            return nil
        }
    }
    
    func mapping(map: Map) {
        error             <- map["error"]
        errorDescription  <- map["error_description"]
    }
    
    class func unknown() -> BMPostApiError {
        return BMPostApiError(error: "unknown")
    }
}
