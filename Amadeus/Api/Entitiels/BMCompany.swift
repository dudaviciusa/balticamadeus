import ObjectMapper

class BMCompany: Mappable {
    
    let name: String
    let catchPhrase: String
    let bs: String
    
    required init?(map: Map) {
        do {
            name = try map.value("name")
            catchPhrase = try map.value("catchPhrase")
            bs = try map.value("bs")
            
        } catch {
            print(error)
            return nil
        }
    }
    
    // Mappable
    func mapping(map: Map) {
    }
}
