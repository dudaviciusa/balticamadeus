import ObjectMapper

class BMAddress: Mappable {
    
    let street: String
    let suite: String
    let city: String
    let zipcode: String
    let geo: BMGeo
    
    
    required init?(map: Map) {
        do {
            street = try map.value("street")
            suite = try map.value("suite")
            city = try map.value("city")
            zipcode = try map.value("zipcode")
            geo = try map.value("geo")
            
        } catch {
            print(error)
            return nil
        }
    }
    
    // Mappable
    func mapping(map: Map) {
    }
}
