import ObjectMapper

class BMGeo: Mappable {
    
    let lat: String
    let lng: String
    
    required init?(map: Map) {
        do {
            lat = try map.value("lat")
            lng = try map.value("lng")
            
        } catch {
            print(error)
            return nil
        }
    }
    
    // Mappable
    func mapping(map: Map) {
    }
}
