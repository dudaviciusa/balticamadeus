import ObjectMapper

class BMUser: Mappable {
    let id: Int
    let name: String
    let username: String
    let email: String
    let phone: String
    let website: String
    let address: BMAddress
    let company: BMCompany
    
    
    required init?(map: Map) {
        do {
            id = try map.value("id")
            name = try map.value("name")
            username = try map.value("username")
            email = try map.value("email")
            phone = try map.value("phone")
            website = try map.value("website")
            address = try map.value("address")
            company = try map.value("company")
            
        } catch {
            print(error)
            return nil
        }
    }
    
    // Mappable
    func mapping(map: Map) {
    }
}
