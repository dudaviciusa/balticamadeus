import ObjectMapper

class BMPost: Mappable {
    public let id: Int
    public let userId: Int
    public let title: String
    public let body: String
    
    required init?(map: Map) {
        do {
            id = try map.value("id")
            userId = try map.value("userId")
            title = try map.value("title")
            body = try map.value("body")
            
        } catch {
            print(error)
            return nil
        }
    }
    
    // Mappable
    func mapping(map: Map) {
    }
}
