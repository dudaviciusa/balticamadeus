import Swinject

class ApiAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(PostApiClient.self) { _ in
            return PostApiClientFactory.createTransferApiClient()
        }.inObjectScope(.container)
    }
}
